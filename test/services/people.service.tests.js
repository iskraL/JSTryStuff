const {
    expect,
} = require('chai');

const {
    init,
} = require('../../app/services/people.service')

describe('PeopleService', () => {
    let service;
    let Model = {
        findById: () => {},
        find: () => {},
    };

    before(() => {
        service = init(Model);
    });

    describe('getPersonById', () => {
        describe('when no person with id', () => {
            beforeEach(() => {
                Model.findById = () => {
                    return Promise.reject();
                };
            });

            it('expect null', () => {
                // When the call (getById) is async, return the promise
                return service.getPersonById('')
                    .then((person) => {
                        expect(person).to.be.null;
                    })
            });
        });

        describe('when person with id', () => {
            const personToReturn = {
                _id: '1',
                name: 'Person Name',
            };

            beforeEach(() => {
                Model.findById = () => {
                    return Promise.resolve(personToReturn);
                }
            });

            it('expect the person', () => {
                return service.getPersonById('1')
                    .then((person) => {
                        expect(person).to.equal(personToReturn);
                    });
            });
        });
    });
});