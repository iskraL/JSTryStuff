angular.module('app')
    .controller('PersonDetailsController', function ($scope, $routeParams, peopleService) {
        const id = $routeParams.id;

        peopleService.getById(id)
            .then(function (person) {
                $scope.person = person;
            });
    });