angular.module('app')
    .controller('PeopleController', function ($scope, peopleService) {
        peopleService.getAll()
            .then(function (people) {
                $scope.people = people;
            });
        $scope.onClick = function (person) {
            console.log(person);
        };
    });