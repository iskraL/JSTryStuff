angular.module('app', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'PeopleController',
                templateUrl: 'templates/people.html',
            })
            .when('/:id', {
                controller: 'PersonDetailsController',
                templateUrl: 'templates/person.html'
            });
    });