angular.module('app')
    .service('peopleService', function ($http) {
        this.getById = function (id) {
            const url = `/people/${id}`;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        };

        this.getAll = function () {
            const url = '/people';
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        };
    });