const express = require('express');
// const service = require('../services/people.service');

const init = (service) => {
    const peopleRouter = new express.Router();

    peopleRouter.get('/', (req, res) => {
            console.log(req.date);
            // service.getTopYoungestPeople(3)
            service.getPeopleWithAgeBetween(26, 40)
                .then((people) => {
                    res.send(people);
                });
        })
        .get('/:id', (req, res) => {
            const id = req.params.id;
            service.getPersonById(id)
                .then((person) => {
                    res.send(person);
                });
        })
        .post('/', (req, res) => {
            const body = req.body;
            service.createPerson(body);
            res.send(body);
        });
    return peopleRouter;
}

module.exports = {
    init,
};