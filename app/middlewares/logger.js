const logger = (req, res, next) => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const date = new Date();
    const route = req.path;
    console.log(`[${date}]: ${route} - ${ip}`);
    next();
};

module.exports = logger;