const date = (req, res, next) => {
    req.date = new Date();
    next();
};

module.exports = date;