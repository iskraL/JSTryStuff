class PeopleService {
    constructor(Person) {
        this.Model = Person;
    }

    getAllPeople() {
        return this.Model.find();
    }

    getPeopleWithAgeBetween(fromAge, toAge) {
        return this.Model.find({
            age: {
                $lte: toAge,
                $gte: fromAge,
            }
        });
    }

    getTopYoungestPeople(count) {
        return this.Model.find({}, [], {
            skip: 0,
            limit: count,
            sort: {
                age: -1
            }
        });
    }

    getPersonById(id) {
        return this.Model.findById(id)
            .catch(() => null);
    }

    createPerson(person) {
        const Person = this.Model;
        const model = new Person(person);
        return model.save();
    }
}

const init = (Person) => {
    return new PeopleService(Person);
};

module.exports = {
    init
};