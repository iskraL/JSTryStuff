const mongoose = require('mongoose');

const {
    Schema,
} = mongoose;

const Person = new Schema({
    name: String,
    age: Number,
    interests: [String],
});

mongoose.model('Person', Person);