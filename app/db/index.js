const mongoose = require('mongoose');
const init = (connectionString) => {
    require('./models/person.model');
    return mongoose.connect(connectionString);
};

module.exports = {
    init
};