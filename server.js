const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const logger = require('./app/middlewares/logger');
const date = require('./app/middlewares/date');

const app = express();

app.set('view engine', 'pug');
app.use(logger);
app.use(date);
// attach middleware
app.use(bodyParser.json());

const staticPath = path.join(__dirname, "static");

app.use(express.static(staticPath))

require('./app/db/index')
    .init('mongodb://localhost/peopledb')
    .then((mongoose) => {
        const Person = mongoose.model('Person');
        //TODO:
        // const repo = require('./repositories/people.repository');
        const service = require('./app/services/people.service')
            .init(Person);
        const peopleRouter = require('./app/routers/people.router')
            .init(service);
        // const peopleRouter = require('./routers/people.router');

        app.use('/people', peopleRouter);
    });

app.listen(3000, () => console.log('Unicorns at :3000'));